/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.common.utils.excel.fieldtype;

import java.util.List;

import com.empire.emsite.common.utils.Collections3;
import com.empire.emsite.modules.sys.entity.Role;
import com.empire.emsite.modules.sys.entity.User;
import com.google.common.collect.Lists;

/**
 * 类RoleListType.java的实现描述：字段类型转换[ TODO 该类需要重写]
 * 
 * @author arron 2017年10月30日 下午1:08:58
 */
public class RoleListType {

    /**
     * 获取对象值（导入）[ TODO 该方法需要重写]
     */
    public static Object getValue(String val, User user) {
        List<Role> roleList = Lists.newArrayList();
        return roleList.size() > 0 ? roleList : null;
    }

    /**
     * 设置对象值（导出）
     */
    public static String setValue(Object val) {
        if (val != null) {
            @SuppressWarnings("unchecked")
            List<Role> roleList = (List<Role>) val;
            return Collections3.extractToString(roleList, "name", ", ");
        }
        return "";
    }

}
